import mongoose from 'mongoose';

const CommentSchema = new mongoose.Schema({
    title: String,
    desc: String
})

module.exports = mongoose.model('Comment', CommentSchema);