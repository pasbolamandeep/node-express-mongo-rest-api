# Node Express MongoDB REST API
Simple REST API to handle CURD operations
### Install Dependencies
[MongoDB](https://docs.mongodb.com/manual/administration/install-community/)
```sh
npm install
```
### Create MongoDB and update the url in index.js
```sh
index.js is pointing to : mongodb://127.0.0.1:27017/commentsManager
```
### Run Server
```sh
npm run start
```
### API Endpoints
#### Get all comments
```sh
type : GET
localhost:1010/api/comments
```
#### Get single comment
```sh
type : GET
localhost:1010/api/comments/<comment_id>
```
#### Add new comment
```sh
type : POST
localhost:1010/api/comments
post data {title:"", desc:""}
```
#### Update comment
```sh
type : PUT
localhost:1010/api/comments/<comment_id>
post data {title:"", desc:""}
```
#### Delete comment
```sh
type : DELETE
localhost:1010/api/comments/<comment_id>
```