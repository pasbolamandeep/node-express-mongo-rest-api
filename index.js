import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import Comment from './app/models/comment';

const app = express();
const router = express.Router();
const port = process.env.PORT || 1010;

mongoose.connect('mongodb://127.0.0.1:27017/commentsManager');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

router.use((req, res, next) => {
    console.log('routing');
    next();
})
router.route('/comments')
.post((req, res) => {
    let comment = new Comment();
    comment.title = req.body.title;
    comment.desc = req.body.desc;
    comment.save((err) => {
        err ? res.send(err) : res.json({message: 'Comment created'});
    })
})
.get((req, res) => {
    Comment.find((err, comments) => {
        err ? res.send(err) : res.json(comments);
    })
});

router.route('/comments/:comment_id')
.get((req, res) => {
    Comment.findById(req.params.comment_id, (err, comment) => {
        err ? res.send(err) : res.json(comment); 
    })
})
.put((req, res) => {
    Comment.findById(req.params.comment_id, (err, comment) => {
        err ? res.send(err) : null;
        comment.title = req.body.title;
        comment.desc = req.body.desc;
        comment.save((err) => {
            err ? res.send(err) : res.json({message: "Comment updated"})
        })
    })
})
.delete((req, res) => {
    Comment.remove({_id: req.params.comment_id}, (err, comment) => {
        err ? res.send(err) : res.json({message: "Comment deleted"})
    })
})
app.use('/api', router);
app.listen(port);
console.log("API running on port "+port)